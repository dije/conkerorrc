// 82follow-relationship.js

dumpln("82follow-relationship");

require("follow-relationship");

// FIXME Replace library version with version that also checks buttons
function document_get_element_by_relationship(doc, patterns, relationship) {
  patterns = patterns[relationship];
  var rel_name = new RegExp(browser_relationship_rel_regexp[relationship], "i");
  var rev_name = new RegExp(browser_relationship_rev_regexp[relationship], "i");

  var elems = doc.getElementsByTagName("link");
  for (var i = 0, n = elems.length; i < n; i++) {
    if (rel_name.test(elems[i].rel) || rev_name.test(elems[i].rev))
      return elems[i];
  }

  elems = doc.getElementsByTagName("a");
  for (i = 0, n = elems.length; i < n; i++) {
    if (rel_name.test(elems[i].rel) || rev_name.test(elems[i].rev))
      return elems[i];
  }

  elems = doc.getElementsByTagName("button");
  for (i = 0, n = elems.length; i < n; i++) {
    if (rel_name.test(elems[i].rel) || rev_name.test(elems[i].rev))
      return elems[i];
  }

  for (var j = 0, p = patterns.length; j < p; ++j) {
    var pattern = patterns[j];
    if (pattern instanceof Function) {
      var elem = pattern(doc);
      if (elem) return elem;
    } else {
      for (i = 0, n = elems.length; i < n; i++) {
        if (pattern.test(elems[i].textContent)) return elems[i];
        var children = elems[i].childNodes;
        for (var k = 0, c = children.length; k < c; k++) {
          if (children[k].alt && pattern.test(children[k].alt)) return elems[i];
        }
      }
    }
  }
  return null;
}
