// Time-stamp: "2019-12-17 09:56:27 phil"

dumpln("14useragent");

require("user-agent-policy");

function user_agent_ubuntu_firefox() {
  return "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:67.0) Gecko/20100101 Firefox/67.0";
}

// Tell Google Calendar that we are Firefox not Conkeror:
user_agent_policy.define_policy(
  "GCal",
  user_agent_firefox(),
  build_url_regexp(($domain = /(.*\.)?google/), ($path = /calendar/))
);

user_agent_policy.define_policy(
  "AReader",
  user_agent_firefox(),
  "read.amazon.com"
);

user_agent_policy.define_policy(
  "GMail",
  user_agent_ubuntu_firefox(),
  build_url_regexp(($domain = /mail\.google/))
);

user_agent_policy.define_policy(
  "Yahoo",
  user_agent_ubuntu_firefox(),
  build_url_regexp(($domain = /mail\.yahoo/))
);
