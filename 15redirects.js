// Time-stamp: "2022-07-19 16:49:29 phil"

dumpln("15redirects");

require("client-redirect");

var debug_redirects = false;
var bogus_page_uri = 'about:sync-log';

define_client_redirect("tpb",
    function (uri) {
      if (! /thepiratebay\.se$/.test(uri.host)) {
        return null;
      }
      if (debug_redirects) {
        dumpln("tpb redirect starting");
        dumpln(`uri.spec: ${uri.spec}`);
        dumpln(`uri.host: ${uri.host}`);
        dumpln(`tpb redirect from: ${uri.spec}`);
        dumpln(uri.spec.replace(/thepiratebay.se/, "thepiratebay.org"));
      }
      return uri.spec.replace(/thepiratebay.se/, "thepiratebay.org");
    });

define_client_redirect("ads",
    function (uri) {
      if (! /http:\/\/www.googleadservices.com\/pagead\/aclk/.test(uri.host)) {
        return null;
      }
      if (debug_redirects) {
        dumpln("ads redirect starting");
        dumpln(`uri.spec: ${uri.spec}`);
        dumpln(`uri.host: ${uri.host}`);
        dumpln(`ads redirect from: ${uri.spec}`);
      }
      if (/%25ds_dest_url%3D/.test(uri.host)) {
        if (debug_redirects) {
          dumpln(uri.spec.replace(/.*%25ds_dest_url%3D(.*)/, "$1"));
        }
        return uri.spec.replace(/.*%25ds_dest_url%3D(.*)/, "$1");
      }
      if (debug_redirects) {
        dumpln(uri.spec.replace(/.*&adurl=(.*)/, "$1"));
      }
      return uri.spec.replace(/.*&adurl=(.*)/, "$1");
    });

function yd (url) {
  const cmd_str = `yd2tv '${url}'`;
  if (debug_redirects) {
    dumpln(cmd_str);
  }
  ph_safe_message(null, `Issuing ${cmd_str}`);
  shell_command_blind(cmd_str);
}

define_client_redirect("yd",
    function (uri) {
      if (! /youtu(be\.com|\.be)$/.test(uri.host)) {
        return null;
      }
      if (debug_redirects) {
        dumpln("yd redirect starting");
        dumpln(`uri.spec: ${uri.spec}`);
        dumpln(`uri.host: ${uri.host}`);
        dumpln(`yd redirect from: ${uri.spec}`);
      }
      yd(uri.spec);
      return bogus_page_uri;
    });

function open_in_chromium (url) {
  const cmd_str = `chromium '${url}'`;
  if (debug_redirects) {
    dumpln(cmd_str);
  }
  ph_safe_message(null, `Issuing ${cmd_str}`);
  shell_command_blind(cmd_str);
  blocking_pause(3000);
  ph_X11_activate(" - Chromium");
}

interactive("open-in-chromium", "Open URL in Chromium",
    function (I) {
      yield open_in_chromium(I.buffer.display_uri_string);
    });
// Browse in Chromium with C-c c
define_key(content_buffer_normal_keymap, "C-c c", "open-in-chromium");

interactive("follow-in-chromium", "Follow link in Chromium",
    function (I) {
      const elem = yield read_browser_object(I);
      var element;
      try {
        element = load_spec(elem);
        if (I.forced_charset) {
          element.forced_charset = I.forced_charset;
        }
      } catch (e) {}
      var e;
      if (elem instanceof load_spec) {
        e = load_spec_element(elem);
      }
      if (! e) {
        e = elem;
      }
      browser_set_element_focus(I.buffer, e, true /* no scroll */);
      yield open_in_chromium(load_spec_uri_string(element));
    },
    $browser_object = browser_object_links,
    $prompt = "Follow in Chromium");

define_key(content_buffer_normal_keymap, "C-c C", "follow-in-chromium");

// "Dummy" redirect that actually opens the URL in Chromium
function define_chromium_redirect(name, regexp) {
  define_client_redirect(name,
    function (uri) {
      if (! regexp.test(uri.host)) {
        return null;
      }
      if (debug_redirects) {
        dumpln(name + " redirect starting");
        dumpln(`uri.spec: ${uri.spec}`);
        dumpln(`uri.host: ${uri.host}`);
        dumpln(`${name} redirect from: ${uri.spec}`);
      }
      open_in_chromium(uri.spec);
      return bogus_page_uri;
    });
}

function ph_kill_bogus_buffer(buffer) {
  if (buffer.display_uri_string == bogus_page_uri) {
    call_after_timeout(function(){kill_buffer(buffer);}, 5000);
  }
}
// FIXME wrong hook
add_hook ("create_buffer_hook", ph_kill_bogus_buffer);

define_chromium_redirect("ticketsolve", /ticketsolve\.com/);
define_chromium_redirect("ee_topup", /myaccount\.ee\.co\.uk/);
define_chromium_redirect("gitlab", /gitlab\.com/);
define_chromium_redirect("reddit", /reddit\.com/);
define_chromium_redirect("Ubiquiti", /ui\.com/);
define_chromium_redirect("JLP", /johnlewis\.com/);
define_chromium_redirect("RaspberryPi", /raspberrypi\.org/);
define_chromium_redirect("Manjaro", /forum\.manjaro\.org/);
define_chromium_redirect("EmacsWiki", /emacswiki\.org/);
define_chromium_redirect("Nyxt", /nyxt\.atlas\.engineer/);
define_chromium_redirect("ArchWiki", /wiki\.archlinux\.org/);
define_chromium_redirect("Org", /orgmode\.org/);
define_chromium_redirect("GNU", /gnu\.org/);
define_chromium_redirect("GeoHack", /geohack\.toolforge\.org/);
define_chromium_redirect("AskUbuntu", /askubuntu\.com/);
define_chromium_redirect("StackExchange", /stackexchange\.com/);
define_chromium_redirect("StackOverflow", /stackoverflow\.com/);
define_chromium_redirect("SuperUser", /superuser\.com/);
define_chromium_redirect("LinuxHint", /linuxhint\.com/);

function open_in_eww (url) {
  const cmd_str = `eww '${url}'`;
  if (debug_redirects) {
    dumpln(cmd_str);
  }
  ph_safe_message(null, `Issuing ${cmd_str}`);
  shell_command_blind(cmd_str);
  blocking_pause(3000);
  ph_X11_activate("*eww: ");
}

interactive("open-in-eww", "Open current URL in eww",
    function (I) {
      yield open_in_eww(I.buffer.display_uri_string, I.window);
    });
// Browse in eww with C-c e
define_key(content_buffer_normal_keymap, "C-c e", "open-in-eww");

// "Dummy" redirect that actually opens the URL in eww
function define_eww_redirect(name, regexp) {
  define_client_redirect(name,
    function (uri) {
      if (! regexp.test(uri.host)) {
        return null;
      }
      if (debug_redirects) {
        dumpln(name + " redirect starting");
        dumpln(`uri.spec: ${uri.spec}`);
        dumpln(`uri.host: ${uri.host}`);
        dumpln(`${name} redirect from: ${uri.spec}`);
      }
      open_in_eww(uri.spec);
      // FIXME Would be better to stay on same page, but... how?
      return bogus_page_uri;
    });
}

define_eww_redirect("SachaChua", /sachachua\.com/);
define_eww_redirect("Th", /thesaurus\.com/);
