// Conkeror preamble

// [[file:../.emacs.d/lisp/ph-engine-mode+.org::*Conkeror preamble][Conkeror preamble:1]]
// Time-stamp: "2022-04-12 17:28:25 phil"
// Section 1x is for URL handling tweaks (?)
dumpln("10webjumps");

require("00common");
require("opensearch.js");
// Conkeror preamble:1 ends here

// Conkeror setup

// [[file:../.emacs.d/lisp/ph-engine-mode+.org::*Conkeror setup][Conkeror setup:1]]
// Smart-select wrapper
// From http://conkeror.org/Tips#SelectionSearches
function ph_plusified_handler(url_template) {
    const placeholder='%s';
    const got_placeholder = url_template.indexOf(placeholder);
    if (got_placeholder == -1) {
        // Just return the same string if it doesn't contain a %s
        return url_template;
    } else if (got_placeholder != url_template.lastIndexOf(placeholder)) {
        throw new interactive_error("Bad webjump url_template");
    }
    const [head, tail] = url_template.split(placeholder);
    return (search_term => `${head}${ph_plusify(search_term)}${tail}`);
}
// selection searches
function create_selection_search (webjump, key) {
    const webjump_selection_search = `${webjump}-selection-search`;
    interactive(webjump_selection_search,
                `Search ${webjump} with selection contents`,
                "find-url-new-buffer",
                $browser_object = function (I) {
                    return `${webjump} ${I.buffer.top_frame.getSelection()}`;});
    define_key(content_buffer_normal_keymap, key.toUpperCase(),
               webjump_selection_search);

    const prompted_webjump_search = `prompted-${webjump}-search`;
    interactive(prompted_webjump_search,
                `Search ${webjump}`,
                function (I) {
                    var term = yield I.minibuffer.read_url($prompt = `Search ${webjump}:`,
                                                           $initial_value = `${webjump} `,
                                                           $select = false);
                    browser_object_follow(I.buffer, FOLLOW_DEFAULT, term);
                });
    define_key(content_buffer_normal_keymap, key, prompted_webjump_search);
}
// examples
// create_selection_search("g","l");
// create_selection_search("wikipedia","w");
// create_selection_search("dictionary","d");
// create_selection_search("myspace","y");
// create_selection_search("amazon","a");
// create_selection_search("youtube","u");

// This is best used in conjunction with the setting:

// minibuffer_read_url_select_initial = false;

define_webjump("startpage-post", "https://www.startpage.com/do/search",
               $post_data = [["query", "%s"], ["cat", "web"],
                             ["cmd", "process_search"], ["language", "english"],
                             ["engine0", "v1all"], ["abp", "-1"]]);

define_webjump("down?", function (url) {
    if (url) {
        return `http://downforeveryoneorjustme.com/${url}`;
    } else {
        return "javascript:window.location.href='http://downforeveryoneorjustme.com/'+window.location.href;";
    }});

define_webjump("tunefind",
               function (search_term) {
                   const b = search_term.indexOf('+s');
                   if ( b == -1 ) {
                       return `https://www.tunefind.com/search/site?q=${search_term}`;
                   } else {
                       const [show, season] = search_term.split('+s');
                       return `https://www.tunefind.com/show/${show}/season-${season}`;
                   }},
               $alternative = "https://www.tunefind.com/search/site?q=%s",
               $doc = "Invoke with 'ShowName+s3' for ShowName season 3");
// Conkeror setup:1 ends here



// #+RESULTS:

// [[file:../.emacs.d/lisp/ph-engine-mode+.org::*Generate Conkeror webjumps][Generate Conkeror webjumps:2]]
define_webjump("Recent",
	"https://www.google.com/search?hl=en&tbo=1&tbs=qdr:y&q=%s");
define_webjump("ServerSettings",
	"https://www.serversettings.email/getemaildata.php?q=%s",
	$alternative="https://www.serversettings.email/");
define_webjump("CommsExpress",
	"https://www.comms-express.com/search/%s/");
define_webjump("PCPartPicker",
	"https://uk.pcpartpicker.com/search/?q=%s");
define_webjump("ArchWiki",
	"https://wiki.archlinux.org/index.php?search=%s&title=Special%3ASearch&fulltext=Search");
define_webjump("ArchForums",
	"http://bbs.archlinux.org/search.php?action=search&show_as=topics&sort_dir=DESC&keywords=%s");
define_webjump("ArchForumAuthor",
	"http://bbs.archlinux.org/search.php?action=search&show_as=topics&sort_dir=DESC&author=%s");
define_webjump("pacman",
	"http://www.archlinux.org/packages/?q=%s");
define_webjump("AUR",
	"http://aur.archlinux.org/packages?K=%s");
define_webjump("StumpCode",
	"http://github.com/sabetts/stumpwm/search?q=%s");
define_webjump("Docker",
	"https://hub.docker.com/search?q=%s&type=image");
define_webjump("MELPA",
	"https://melpa.org/#/?q=%s");
define_webjump("trans_de",
	"http://translate.google.com/translate_t#de|en|%s");
define_webjump("trans_fr",
	"http://translate.google.com/translate_t#fr|en|%s");
define_webjump("anagram",
	"http://wordsmith.org/anagram/anagram.cgi?anagram=%s&t=1000&a=n");
define_webjump("firefox_addons",
	"https://addons.mozilla.org/en-US/firefox/search?q=%s");
define_webjump("OL",
	"http://search.gmane.org/?query=%s&group=gmane.emacs.orgmode");
define_webjump("youtube_user",
	"http://youtube.com/profile_videos?user=%s");
define_webjump("ratpoison",
	"http://ratpoison.wxcvbn.org/?search=%s");
define_webjump("ixquick",
	"https://ixquick.com/do/metasearch.pl?query=%s");
create_selection_search("ixquick", "h");
define_webjump("startpage_image",
	"https://eu.startpage.com/do/search?query=%s&cat=pics&cmd=process_search&language=english");
define_webjump("Amazon",
	"http://www.amazon.co.uk/exec/obidos/external-search/?field-keywords=%s&mode=blended");
define_webjump("bashfaq",
	"http://mywiki.wooledge.org/BashFAQ?action=fullsearch&context=180&value=%s&fullsearch=Text",
	$alternative="http://mywiki.wooledge.org/BashFAQ");
define_webjump("lispcode",
	"http://code.ohloh.net/search?s=%s&fl=Lisp");
define_webjump("js",
	"http://www.koders.com/?s=%s&li=*&la=JavaScript");
define_webjump("cliki",
	"http://www.cliki.net/site/search?query=%s");
define_webjump("lispgames",
	"http://lispgames.org/index.php?search=%s&go=Go&title=Special%3ASearch");
define_webjump("Vimeo",
	"http://vimeo.com/search?q=%s");
define_webjump("openstreetmap",
	"http://www.openstreetmap.org/search?query=%s");
define_webjump("AllMusic",
	"http://www.allmusic.com/search/all/%s");
define_webjump("validate",
	"http://validator.w3.org/check?uri=%s&charset=%28detect+automatically%29&doctype=Inline&group=0");
define_webjump("validate_css",
	"http://jigsaw.w3.org/css-validator/validator?uri=%s&profile=css21&usermedium=all&warning=1&lang=en");
define_webjump("gmane",
	"http://gmane.org/find.php?list=%s");
define_webjump("isohunt",
	"https://isohunt.to/torrents/?ihq=%s");
define_webjump("snopes",
	"http://www.snopes.com/search/?q=%s");
define_webjump("BBC",
	"http://www.bbc.co.uk/search?q=%s");
define_webjump("codesearch",
	"http://codesearch.debian.net/search?q=%s");
define_webjump("Canary",
	"https://www.thecanary.co/?s=%s");
define_webjump("Gumtree",
	"https://www.gumtree.com/search?q=%s&search_location=uk&sort=price_lowest_first");
define_webjump("Th",
	"http://www.thesaurus.com/browse/%s");
define_webjump("argos",
	"https://www.argos.co.uk/search/%s");
define_webjump("CreativeCommons",
	"https://ccsearch.creativecommons.org/?search_fields=title&search_fields=creator&search_fields=tags&search=%s");
define_webjump("eBay",
	"https://www.ebay.co.uk/sch/items/?_nkw=%s");
define_webjump("UrbanDictionary",
	"https://www.urbandictionary.com/define.php?term=%s");
define_webjump("commandlinefu",
	"http://www.commandlinefu.com/commands/matching/%s");
define_webjump("Sacha",
	"https://sachachua.com/blog/?s=%s");
define_webjump("DC",
	"https://dc.fandom.com/wiki/Special:Search?query=%s");
define_webjump("Discogs",
	"https://www.discogs.com/search/?q=%s&type=all");
define_webjump("ELPA",
	"https://elpa.gnu.org/packages/%s.html");
define_webjump("EW",
	"https://duckduckgo.com/?q=%s+site%3Aemacswiki.org&ia=web",
	$alternative="http://www.emacswiki.org/");
create_selection_search("EW", "a");
define_webjump("Forex",
	"https://api.exchangeratesapi.io/latest?base=GBP&symbols=%s");
define_webjump("GitHub",
	"https://github.com/search?ref=simplesearch&q=%s");
define_webjump("Gutenberg",
	"http://www.gutenberg.org/ebooks/search/?query=%s");
define_webjump("HTTP",
	"https://httpstatuses.com/%s");
define_webjump("HackerNews",
	"https://www.hnsearch.com/search#request/submissions&q=%s&start=0");
define_webjump("Homeward",
	"https://www.google.com/maps/dir/home/%s");
define_webjump("Wiktionary",
	"https://en.wiktionary.org/w/index.php?search=%s&title=Special:Search&profile=advanced&fulltext=1&ns0=1");
define_webjump("GoogleImages",
	"https://www.google.com/search?tbm=isch&q=%s");
define_webjump("ConkerorWiki",
	"http://conkeror.org/?action=fullsearch&context=60&value=%s&fullsearch=Text");
define_webjump("Google",
	"https://www.google.com/search?q=%s");
define_webjump("GMaps",
	"http://maps.google.com/maps?q=%s");
create_selection_search("GMaps", "p");
define_webjump("MozDev",
	"https://developer.mozilla.org/Special:Search?search=%s&type=fulltext&go=Search");
define_webjump("SymbolHound",
	"http://symbolhound.com/?q=%s");
define_webjump("ESE",
	"https://emacs.stackexchange.com/search?q=%s");
define_webjump("StackOverflow",
	"https://stackoverflow.com/search?q=%s",
	$alternative="http://stackoverflow.com");
define_webjump("worg",
	"https://www.google.com/cse?cx=002987994228320350715%%3Az4glpcrritm&q=%s&sa=Search&siteurl=orgmode.org%%2Fworg",
	$alternative="http://orgmode.org/worg/");
create_selection_search("worg", "w");
define_webjump("Pirate",
	"https://thepiratebay.org/search.php?q=%s",
	$alternative="https://thepiratebay.news/search.php?q=%s");
define_webjump("WikiQuote",
	"https://en.wikiquote.org/wiki/Special:Search?go=Go&search=%s&ns0=1");
define_webjump("Rarbg",
	"https://rarbg.to/torrents.php?search=%s&order=size&by=ASC");
define_webjump("Rosetta",
	"https://www.rosettacode.org/mw/index.php?title=Special%3ASearch&search=%s&go=Go");
define_webjump("StartPage",
	"https://startpage.com/do/search?query=%s&cat=web&cmd=process_search&language=english&engine0=v1all&abp=-1");
define_webjump("SuperUser",
	"https://superuser.com/search?q=%s");
define_webjump("site",
	"javascript:location='https://www.google.com/search?num=100&q=site:'+escape(location.hostname)+'+%s'");
define_webjump("ETTV",
	"https://www.ettv.tv/torrents-search.php?search=%s");
define_webjump("eBuyer",
	"https://www.ebuyer.com/search?q=%s");
define_webjump("DuckDuckGo",
	"https://duckduckgo.com/?q=%s");
define_webjump("Marvel",
	"https://marvel.fandom.com/wiki/Special:Search?query=%s");
define_webjump("IMDB",
	"https://www.imdb.com/find?s=all&q=%s",
	$alternative="https://imdb.com");
define_webjump("Wikipedia",
	"https://en.wikipedia.org/wiki/Special:Search?search=%s");
define_webjump("cex",
	"https://uk.webuy.com/search?stext=%s&sortBy=sellprice&sortOrder=asc");
define_webjump("Translate",
	"https://translate.google.com/?source=osdd#auto|en|%s");
define_webjump("YouTube",
	"https://www.youtube.com/results?search_query=%s");
create_selection_search("YouTube", "y");
define_webjump("BroadbandBuyer",
	"https://www.broadbandbuyer.com/search/?q=%s");
define_webjump("Zoo",
	"https://zooqle.com/search?q=%s&s=sz&v=t&sd=a");
// Generate Conkeror webjumps:2 ends here
