// Time-stamp: "2022-12-02 16:03:10 phil"
// Section 4x is for inter-application communications tweaks
dumpln("40emacs");

require("00common");

/*
 In emacs, configure conkeror as the external browser:

(setq browse-url-browser-function 'browse-url-generic
      browse-url-generic-program "/path/to/conkeror")
*/

// Avoid blocking from ssh waiting for tty input (passphrase)
setenv("SSH_ASKPASS", "/usr/bin/ssh-askpass");
require("external-editor");
editor_shell_command = "timeout --signal=9 5m " +
    ph_where_editors_executable("ecc");
var emacsclient_executable = "timeout --signal=9 5m " +
    ph_where_editors_executable("emacsclient");

view_source_use_external_editor = true;
// edit-current-field-in-external-editor is on C-i when a text field/area has
// focus


// To make temp files used by external editor end in ".conkeror_field.md":
external_editor_extension_overrides.set("text/plain", "conkeror_field.md");

// Advise `external_editor_make_base_filename' to prefix "conkeror-edit-"
var default_external_editor_make_base_filename =
    (default_external_editor_make_base_filename ||
     external_editor_make_base_filename);
external_editor_make_base_filename = function (elem, top_doc) {
    return "conkeror-edit-" +
        default_external_editor_make_base_filename(elem, top_doc);
};


// org-protocol stuff
function meta_org_capture(window, search_params) {
  const cmd_str = `${emacsclient_executable} 'org-protocol://capture?${search_params}'`;
  dumpln(cmd_str);
  ph_safe_message(window, `Issuing ${cmd_str}`);
  shell_command_blind(cmd_str);
  blocking_pause(3000);
  ph_X11_activate("org-capture-pop-frame");
}

interactive("org-capture", "Clip url, title, and selection to capture via org-protocol",
    function (I) {
      // No support for URLSearchParams in xulrunner WTF
      const search_params = [
        ["template", "w"],
        ["url",      I.buffer.display_uri_string],
        ["title",    I.buffer.document.title],
        ["body",     I.buffer.top_frame.getSelection()]
      ].reduce((prev, curr) =>
        `${prev}${prev && '&'}${curr[0]}=${ph_encodeURIComponent(curr[1])}`, '');
      yield meta_org_capture(I.window, search_params);
    });
// capture with C-c t, same as in emacs
define_key(content_buffer_normal_keymap, "C-c t", "org-capture");

// Programmes to download
function org_capture_programme (plist, window) {
  meta_org_capture (window, `template=p&body=${plist}`);
}

var last_season = last_season || "1";
var ph_well_known_title_patterns = ph_well_known_title_patterns || [
  /^(.*) \(season (\d+)\)/,
  /^(.*)#Season_(\d+)/,
  /^(.*)#Series_(\d+)/,
  /.*List of (.*) episodes.*/,
  /^(.*) \((?:\d{4} )?TV series\)/,
  /^(.*) - Wikipedia/
];

//dumpln (`****** title: ${title} *******`);
//dumpln (`****** location: ${location} *******`);

function ph_try_named_anchor (url) {
  const location = url.replace(/^https?:\/\/(..\.)?wikipedia\.org\/wiki\//, '');
  var location_season = null;
  ph_well_known_title_patterns.find(pattern => location_season = location.match(pattern));
  if (location_season && location_season.length == 3) {
    return location_season[2];
  } else {
    return null;
  }
}

interactive("org-capture-programme",
    "Clip url, title, and episode details to capture via org-protocol",
    function (I) {
      const title = I.buffer.document.title;
      var show_season = null;
      ph_well_known_title_patterns.find(pattern => show_season = title.match(pattern));
      const show = show_season[1];
      var season = null;
      if (show_season.length == 3) {
        season = show_season[2];
      } else if (! (season = ph_try_named_anchor(I.buffer.current_uri.spec))) {
        season =
          (yield I.minibuffer.read($prompt = `${show} season: `,
                                   $initial_value = last_season,
                                   $select = true,
                                   $history = "seasons"));
        last_season = season;
      }
      const selection = I.buffer.top_frame.getSelection();
      const episode_fields = selection.toString().split('\t');
      const episode_number = episode_fields[0];
      const episode_date = ph_next_day_org_date(episode_fields[episode_fields.length - 1]);
      const plist = `(:show "${show}" :season ${season} :episode-number ${episode_number} ` +
            `:episode-date "${episode_date}")`;
      yield org_capture_programme(ph_encodeURIComponent(plist), I.window);
    });
// capture with C-c p, same as in emacs
define_key(content_buffer_normal_keymap, "C-c p", "org-capture-programme");


function org_store_link (url, title, window) {
    const cmd_str = emacsclient_executable +
              ` 'org-protocol://store-link://${url}/${title}'`;
    ph_safe_message(window, 'Issuing ' + cmd_str);
    // TODO log the command to a file
    shell_command_blind(cmd_str);
}

interactive(
    "org-store-link",
    "Stores [[url][title]] as org link and copies url to emacs kill ring",
    function (I) {
        org_store_link(
            ph_encodeURIComponent(I.buffer.display_uri_string),
            ph_encodeURIComponent(I.buffer.document.title),
            I.window
        );
    });

define_key(content_buffer_normal_keymap, "C-c l", "org-store-link");
