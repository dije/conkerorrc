dumpln("50os");

function os_specific_config(stem) {
  const os_type = getenv("OSTYPE");
  const config_dir = get_home_directory().clone();
  config_dir.appendRelativePath(`.conkerorrc/${os_type}`);
  return make_file(`${config_dir.path}/${stem}.${os_type}.js`);
}

load(os_specific_config("50os"));
