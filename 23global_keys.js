// Time-stamp: "2021-07-09 07:36:18 phil"

dumpln("50global_keys");

require("text.js");

function ph_insert_char(field, char) {
  modify_region(field, function() {
    return char;
  });
}
function insert_pound_sterling(field) {
  ph_insert_char(field, "\u{000A3}");
}
interactive("insert-pound-sterling", "Insert '£'.", function(I) {
  call_on_focused_field(I, insert_pound_sterling, true);
});

define_key(text_keymap, "C-x 8 l", "insert-pound-sterling");

// Make keys 1 thru 9 switch to buffer with that 1-based index (represented
// internally as 0-based).
function define_switch_buffer_key(key, buf_num) {
  define_key(default_global_keymap, key, I =>
    switch_to_buffer(I.window, I.window.buffers.get_buffer(buf_num))
  );
}
for (i = 0; i < 9; ++i) {
  define_switch_buffer_key(String(i + 1), i);
}
// Make key 0 switch to highest-index buffer.
define_key(default_global_keymap, "0", function(I) {
  const last_buffer_index = I.window.buffers.buffer_list.length - 1;
  switch_to_buffer(I.window, I.window.buffers.get_buffer(last_buffer_index));
});

define_key(default_global_keymap, "I", I =>
  browser_object_follow(I.buffer, OPEN_NEW_BUFFER, "https://my.if.com")
);

provide("50global_keys");
