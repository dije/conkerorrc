// 27minibuffer.js
// Time-stamp: "2019-05-29 16:08:06 phil"

dumpln("27minibuffer");

minibuffer_auto_complete_default = true;
url_completion_use_history = true; // should work since bf05c87405
url_completion_use_bookmarks = true;

// To enable fetching of favicons:
require("favicon");

// To enable favicons in the mode-line:
add_hook("mode_line_hook", mode_line_adder(buffer_icon_widget), true);

// To enable favicons in the read_buffer completions list:
read_buffer_show_icons = true;

// To enable download feedback in the mode-line:
add_hook("mode_line_hook", mode_line_adder(downloads_status_widget));

// To enable window buffer count in the mode-line:
add_hook("mode_line_hook", mode_line_adder(buffer_count_widget), true);

// To invoke an alert, enter at a M-: prompt:
//  get_recent_conkeror_window().alert("hello world");

// Modify the code from modules/mode-line.js
clock_widget.prototype.update = function() {
  const now = new Date();
  const year = now.toLocaleDateString("en-GB", { year: "2-digit" });
  const month = now.toLocaleDateString("en-GB", { month: "2-digit" });
  const day = now.toLocaleDateString("en-GB", { day: "2-digit" });
  const dow = now.toLocaleDateString("en-GB", { weekday: "short" }).slice(0, 2);
  const hour = now.toLocaleTimeString("en-GB", { hour: "2-digit" });
  // When does "2-digit" not mean 2 digits? WTF?!
  var min = now.toLocaleTimeString("en-GB", { minute: "2-digit" });
  if (min.length === 1) {
    min = `0${min}`;
  }
  this.view.text = `${year}${month}${day}${dow}${hour}${min}`;
  const seconds = now.getSeconds();
  const millis = now.getMilliseconds();
  if (seconds > 0 || millis > 100) {
    this.window.clearTimeout(this.timer_ID);
    const next_update = 60000 - (seconds * 1000 + millis);
    this.timer_ID = this.window.setTimeout(this.do_update, next_update);
    this.timer_timeout = true;
  } else if (this.timer_timeout) {
    this.window.clearTimeout(this.timer_ID);
    this.timer_ID = this.window.setInterval(this.do_update, 60000);
    this.timer_timeout = false;
  }
};
