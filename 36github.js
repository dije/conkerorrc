// Time-stamp: "2021-05-05 11:07:25 phil"

dumpln("36github");

// Disabled 'cos github-mode sucks
//require("github");

// From http://ericjmritz.name/2013/07/19/cloning-from-github-with-one-key-in-conkeror
interactive("github-clone-current-buffer-url", null,
  function (I) {
    const meta = I.buffer.document.querySelector("meta[property='og:url']");
    if (meta) {
      const clone_command = make_file("~/bin/git-uri-clone").path;
      const uri = meta.getAttribute("content").toString();
      yield shell_command(`${clone_command} ${uri}`);
      I.minibuffer.message("Cloned " + uri);
    }
    else {
      I.minibuffer.message("Cannot find URL to clone");
    }
  }
);

// And now there's no github_keymap
//define_key(github_keymap, "C", "github-clone-current-buffer-url");
